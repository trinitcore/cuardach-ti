const webpack = require('webpack');
const path = require('path');

const config = {
  // Entry points to the project
  entry: {
    main: [
      // only- means to only hot reload for successful updates
      'webpack/hot/only-dev-server',
      './src/Renderer.js',
    ],
  },
  // Server Configuration options
  devServer: {
    contentBase: 'src', // Relative directory for base of server
    hot: false, // Live-reload
    inline: true,
      watchOptions: {
          aggregateTimeout: 300,
          poll: 4000
      },
    port: 3000, // Port Number
    host: 'localhost', // Change to '0.0.0.0' for external facing server
        historyApiFallback: true
  },
  devtool: 'eval',
  output: {
    filename: 'app.js',
  },
    resolve: {
        modules: [
            path.resolve(__dirname, 'trinreact'), 'node_modules',
        ]
    },
  plugins: [
    // Enables Hot Modules Replacement
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
      rules: [
          {
              test: /\.js$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
              query: {
                  cacheDirectory: true,
              },
          },
          {
              test: /\.(jpe?g|png|gif|svg)$/i,
              loaders: [
                  'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                  'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
              ]
          },
          {
              test: /\.js$/,
              loader: 'babel-loader',
              include: [
                  path.resolve(__dirname, 'src'),
                  // webpack-dev-server#1090 for Safari
                  /node_modules\/webpack-dev-server/
              ]
          },
          {
              test: /\.css$/,
              use: [ 'style-loader', 'css-loader' ]
          }
      ],
  },
};

module.exports = config;
