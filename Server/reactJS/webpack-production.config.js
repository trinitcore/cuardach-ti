const webpack = require('webpack');
const path = require('path');
const TransferWebpackPlugin = require('transfer-webpack-plugin');

const config = {
  entry: {
    main: [
      './src/Renderer.js',
    ],
  },
  // Render source-map file for final build
  devtool: 'source-map',
    devServer: {
        historyApiFallback: true
    },
  // output config
  output: {
    path: path.resolve(__dirname, '../resources/web/main'), // Path of output file
    filename: 'app.js', // Name of output file
      publicPath: '/'
  },
    resolve: {
        modules: [
            path.resolve(__dirname, 'trinreact'), 'node_modules',
        ]
    },
  plugins: [
    // Define production build to allow React to strip out unnecessary checks
    new webpack.DefinePlugin({
      'process.env':{
        'NODE_ENV': JSON.stringify('production')
      }
    }),
      // Minify the bundle
      new webpack.optimize.UglifyJsPlugin({
          sourceMap: true,
      })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true,
        },
      },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loaders: [
                'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
            ]
        },
        {
            test: /\.css$/,
            use: [ 'style-loader', 'css-loader' ]
        }
    ],
  },
};

module.exports = config;
