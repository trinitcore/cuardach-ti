import React from "react";
import {Form, ProgressSessionFrame} from "trinreact"
import FormInterface from "../../uiComponents/FormInterface";
import Button from "../../uiComponents/Button";
import SelectField from "../../uiComponents/SelectField";
import TextInput from "../../uiComponents/TextInput";
import "./Dashboard.css"
import SpecificValuesPairs from "../../uiComponents/SpecificValuesPairs";

class Dashboard extends ProgressSessionFrame {

    constructor(props) {
        super(props, {
            counties: null,
            newTowns: null,
            towns: {}
        })
    }

    componentDidMount() {
        this.reload();
    }

    reload() {
        this.requestWithProgress(link("admin.Dashboard","render")).performRequest()
    }

    errorRender(message, status) {
        return (
            <h1>An error occurred. The developer will have to fix this.</h1>
        )
    }

    successRender(data, status) {
        console.log(data);
        return (
            <div>
                <Button label={"Sign Out"} onClick={this.signOut} />

                <h2>Active computers</h2>


                <h2>Create new search criteria</h2>
                <Form URL={link("admin.Dashboard","addSearchCriteria")} postRequestHandler={() => {
                    this.reload();
                    return true;
                }}>
                    <div className={"criteria-wrapper"}>
                        <div>
                            <SelectField label="County" name={"countyID"} optional={true} values={data.counties} valueSelected={(value) => {
                                this.setState({newTowns: value.towns})
                            }}/>
                        </div>
                        <div>
                            {this.state.newTowns != null ? <SelectField label="Town" name={"townID"} optional={true} values={this.state.newTowns}/> : null}
                        </div>
                        <div>
                            <TextInput label="Min Price" name={"minPrice"} optional={true} numeric={true} />
                            <TextInput label="Max Price" name={"maxPrice"} optional={true} numeric={true} />
                        </div>
                    </div>
                    <FormInterface>
                        <Button label={"Create Criteria"} submit={true}/>
                    </FormInterface>
                </Form>

                <h2>Current search criteria(s)</h2>
                {data.searchCriterias.map(criteria => {
                    return <Form URL={link("admin.Dashboard","setSearchCriteria")} additionalParameters={{ID: criteria.ID}} postRequestHandler={() => {
                        this.reload();
                        return true;
                    }}>
                        <h5>Search Properties</h5>
                        <div className={"criteria-wrapper"}>
                            <div>
                                <SelectField value={criteria.countyID}
                                    label="County" name={"countyID"} optional={true} values={data.counties} valueSelected={(value) => {
                                        let towns = this.state.towns;
                                        towns[criteria.ID] = value.towns;
                                        this.setState({towns: towns})
                                }}/>
                            </div>
                            <div>
                                <SelectField value={criteria.townID} label="Town" name={"townID"} optional={true} values={this.state.towns[criteria.ID] || data.counties.find(x => x.ID === criteria.countyID).towns}/>
                            </div>
                            <div>
                                <TextInput value={criteria.minPrice} label="Min Price" name={"minPrice"} optional={true} numeric={true} />
                                <TextInput value={criteria.maxPrice} label="Max Price" name={"maxPrice"} optional={true} numeric={true} />
                            </div>
                        </div>
                        <h5>Specific Properties</h5>
                        <SpecificValuesPairs specificFields={data.specificFields} name={"specificFields"} values={criteria.specificCriterias} />
                        <FormInterface>
                            <Button label={"Set Criteria"} submit={true}/>
                            <Button label={"Remove Criteria"} submit={true} alternativeURL={link("admin.Dashboard","removeSearchCriteria")}/>
                            <Button label={"View results"} onClick={() => { this.props.history.push("/admin/activeSearchCriteriaResults/"+criteria.ID) }}/>
                        </FormInterface>
                    </Form>
                })}
            </div>
        )
    }

}

export default Dashboard;