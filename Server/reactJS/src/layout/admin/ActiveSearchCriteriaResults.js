import React from "react";
import {ProgressSessionFrame,ModuleTableLayout} from "trinreact"

class ActiveSearchCriteriaResults extends ProgressSessionFrame {

    constructor(props) {
        super(props, {})
    }

    componentDidMount() {
        this.requestWithProgress(link("admin.ActiveSearchCriteriaResults","render"),{ID : parseInt(this.props.match.params.ID)}).performRequest()
    }

    errorRender(message, status) {
        return (
            <div>
                <h1>An error occurred. The developer will have to fix this.</h1>
                <h6>Message for developer {message}</h6>
            </div>
        )
    }

    structure = [
        "url",
        "initialRegisterTime",
        "price",
        "address",
        "uniqueIdentifier",
        "beds",
        "bathrooms",
        "BER",
        "size",
        "houseType",
        "dimensions",
        "entryLastRegisteredTime",
        "unlisted"
    ];

    titles = [
        "URL",
        "Initial Register Time",
        "Price",
        "Address",
        "Unique Identifier",
        "Beds",
        "Bathrooms",
        "BER",
        "Size",
        "House Type",
        "Dimensions",
        "Entry Last Registered Time",
        "Unlisted"
    ];

    successRender(data, status) {
        return (
            <div>
                <ModuleTableLayout moduleTitles={this.titles} modules={data.allResults} moduleStructure={this.structure} title={"Results"}/>
            </div>
        )
    }

}

export default ActiveSearchCriteriaResults;