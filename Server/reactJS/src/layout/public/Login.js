import React from "react";
import {LoginFrame} from "trinreact"
import {Form} from "trinreact"
import TextInput from "../../uiComponents/TextInput";
import FormInterface from "../../uiComponents/FormInterface";
import Button from "../../uiComponents/Button";

class Login extends LoginFrame {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.formSubmission}>
                    <TextInput label={"Username"} name={"username"}/>
                    <TextInput label={"Password"} name={"password"}/>
                    <FormInterface>
                        <Button label={"Login"} submit={true}/>
                    </FormInterface>
                </Form>
            </div>
        )
    }

}

export default Login;