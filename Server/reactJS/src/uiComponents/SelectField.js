import React from "react";
import {TrinTextInput} from "trinreact";
import PropTypes from "prop-types";

class SelectField extends TrinTextInput {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        if (this.props.value != null) {
            this.refs.selectBox.value = this.props.value;
            this.setValue(this.props.value)
        } else {
            this.setValue(this.props.values[0].ID)
        }
    }

    render() {
        return (
            <label>
                {this.props.label}:
                <select ref={"selectBox"} onChange={(e) => {
                    let ID = e.target.value;
                    let value = this.props.values.find(x => x.ID == ID);
                    this.setValue(value.ID);
                    if (this.props.valueSelected != null) {
                        this.props.valueSelected(value)
                    }
                }}>
                    {this.props.values.map((value) => {
                        return <option value={value.ID}>{value.label}</option>
                    })}
                </select>
            </label>
        )
    }

}

SelectField.defaultProps = {
    name: null,
    valueSelected: null,
    value: null,
    optional: false,
    numeric: true
};
SelectField.propTypes = {
    values: PropTypes.string.isRequired,
    name: PropTypes.string,
    label: PropTypes.string.isRequired,
    valueSelected: PropTypes.func,
    value: PropTypes.any,
    optional: PropTypes.boolean
};
export default SelectField;