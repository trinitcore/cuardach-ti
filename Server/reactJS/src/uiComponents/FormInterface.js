import React from "react";
import {TrinFormInterface} from "trinreact"
import PropTypes from "prop-types"

class FormInterface extends TrinFormInterface {

    constructor(props) {
        super(props)
    }

    textRender(message) {
        return (
            <h5>{message}</h5>
        )
    }

}

FormInterface.defaultProps = {
    hideButtonsOnOKStatus: false,
    hideButtonsOnNeutralStatus: false,
    hideButtonsOnWarningStatus: false,
    hideButtonsOnErrorStatus: false
};

FormInterface.propTypes = {
    hideButtonsOnOKStatus: PropTypes.bool,
    hideButtonsOnNeutralStatus: PropTypes.bool,
    hideButtonsOnWarningStatus: PropTypes.bool,
    hideButtonsOnErrorStatus: PropTypes.bool,
};

export default FormInterface;