import React from "react";
import {TrinTextInput} from "trinreact"
import PropTypes from "prop-types"

class TextInput extends TrinTextInput {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <input type={"text"} placeholder={this.props.label} onChange={this.setInputValue} value={this.state.value} />
        )
    }

}

TrinTextInput.defaultProps = {
    value: null,
    name: null,
    numeric: false,
    optional: false
};
TrinTextInput.propTypes = {
    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string.isRequired,
    numeric: PropTypes.boolean,
    optional: PropTypes.boolean
};
export default TextInput;