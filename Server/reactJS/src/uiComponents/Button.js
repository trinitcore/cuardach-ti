import React from "react";
import {TrinButton} from "trinreact"
import PropTypes from "prop-types"

class Button extends TrinButton {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <input type={"button"} value={this.props.label} onClick={this.onClick}/>
        )
    }

}

Button.defaultProps = {
    additionalParameters: {},
    submit: false,
    clearAll: false,
    onClick: () => {}
};

Button.propTypes = {
    label: PropTypes.string.isRequired,
    additionalParameters: PropTypes.object,
    alternativeURL: PropTypes.string,
    submit: PropTypes.bool,
    clearAll: PropTypes.bool,
    onClick: PropTypes.func
};

export default Button;