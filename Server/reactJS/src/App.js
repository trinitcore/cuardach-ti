import React, {Component} from 'react';
import {AppController} from "trinreact"
import {Route} from "trinreact"
import {RouteBundle} from "trinreact"
import {SessionRouteBundle} from "trinreact"
import {ComponentRoute} from "trinreact"
import {Theme} from "trinreact";

import Login from "./layout/public/Login";
import Dashboard from "./layout/admin/Dashboard";
import ActiveSearchCriteriaResults from "./layout/admin/ActiveSearchCriteriaResults";

class App extends AppController {

    defineTheme() {
        return new Theme()
            .defineProgressIndicator(<div>Processing your request</div>)
    }

    defineLoginComponent() {
        return Login
    }

    defineNavigationBars() {
        return [
            new ComponentRoute("/admin",<h2>Administrative dashboard</h2>)
        ]
    }

    defineSideBars() {
        return [];
    }

    defineSideDrawers() {
        return []
    };

    defineRoutes() {
        return [
            new SessionRouteBundle("admin",[
                new Route("dashboard",Dashboard),
                new Route("activeSearchCriteriaResults/:ID",ActiveSearchCriteriaResults)
            ])
        ]
    }
}

export default App;
