package com.trinitcore.cuardachti.server.html

import com.trinitcore.asd.responseTools.status.html.subNodes.ScriptNode
import com.trinitcore.asd.responseTools.status.html.subNodes.StylesheetNode
import com.trinitcore.asd.responseTools.support.ReactJSTheme
import com.trinitcore.asd.servlet.ReactJSServlet
import com.trinitcore.asd.servlet.UploadedWebResourcesServlet
import com.trinitcore.asd.servlet.WebResourcesServlet
import javax.servlet.annotation.WebServlet
import kotlin.reflect.KClass

@WebServlet("/*")
class MainReactJSServlet : ReactJSServlet() {
    override fun additionalScripts(): Array<ScriptNode> = arrayOf()

    override fun additionalStylesheets(): Array<StylesheetNode> = arrayOf()

    override fun title(): String = "Cuardach Ti"

    override fun uploadedResourceServlet(): KClass<out UploadedWebResourcesServlet> = MainUploadedWebResourceServlet::class

    override fun webResourceServlet(): KClass<out WebResourcesServlet> = MainWebResourceServlet::class
}