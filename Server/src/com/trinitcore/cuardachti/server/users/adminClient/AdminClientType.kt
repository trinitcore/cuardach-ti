package com.trinitcore.cuardachti.server.users.adminClient

import com.trinitcore.asd.user.CommonUserType
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.configurations.Column
import com.trinitcore.cuardachti.server.users.admin.AdminType
import com.trinitcore.cuardachti.server.users.client.ClientType
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.ModuleTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import kotlin.reflect.KClass

class AdminClientType : CommonUserType(AdminType::class, ClientType::class) {
    override fun tables(): Array<GenericTableBuilder> = arrayOf(

    )

    override fun userColumns(): Array<Column>? = null

    override fun configureUserTable(userTableBuilder: TableBuilder): Boolean {
        return true
    }

}