package com.trinitcore.cuardachti.server.users

import com.trinitcore.asd.servlet.AdvancedServlet
import com.trinitcore.asd.user.PublicUserType
import com.trinitcore.cuardachti.server.html.MainReactJSServlet
import com.trinitcore.cuardachti.server.rest.publicREST.Houses
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.ModuleTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.sqlv2.queryUtils.associationV2.Associating
import com.trinitcore.sqlv2.queryUtils.associationV2.table.RowAssociation
import com.trinitcore.sqlv2.queryUtils.associationV2.table.RowsAssociation
import kotlin.reflect.KClass

class PublicType : PublicUserType(MainReactJSServlet::class, Houses::class) {

    override fun publicTables(): Array<GenericTableBuilder> = arrayOf(
            TableBuilder("activesearchcriterias")
                    .addAssociation(RowsAssociation("specificcriterias",Associating("ID","specificCriterias","activeSearchCriteriaID"))
                            .addAssociation(RowAssociation("specificfields",
                                    Associating("specificFieldID","specificField","ID")
                            )))
                    .addAssociation(RowAssociation("counties",Associating("countyID","county","ID")))
                    .addAssociation(RowAssociation("towns",Associating("townID","town","ID"))),
            TableBuilder("houses"),
            TableBuilder("counties")
                    .addAssociation(RowsAssociation("towns", Associating("ID","towns","countyID").blankRowsIfMatchNotFound())),
            TableBuilder("specificfields"),
            TableBuilder("specificcriterias")
                    .addAssociation(RowAssociation("specificfields",
                            Associating("specificFieldID","specificField","ID")
                    ))
    )

}