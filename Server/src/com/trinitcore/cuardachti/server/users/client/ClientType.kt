package com.trinitcore.cuardachti.server.users.client

import com.trinitcore.asd.resourceTools.email.contents.EmailContent
import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder

class ClientType : UserType<Client>(ID = 0) {
    override fun reactJSAllowedPages(): Array<String> = arrayOf()

    override fun reactJSLoginRedirectPage(): String = ""

    override fun configureUserTable(userTableBuilder: TableBuilder) {

    }

    override fun confirmationEmailContent(userData: User): EmailContent? = null

    override fun tables(): Array<GenericTableBuilder> = arrayOf(

    )
}