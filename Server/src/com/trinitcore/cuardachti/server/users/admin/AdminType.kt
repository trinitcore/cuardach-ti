package com.trinitcore.cuardachti.server.users.admin

import com.trinitcore.asd.resourceTools.email.contents.EmailContent
import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.cuardachti.server.rest.admin.ActiveSearchCriteriaResults
import com.trinitcore.cuardachti.server.rest.admin.Dashboard
import com.trinitcore.sqlv2.queryObjects.builders.GenericTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder

class AdminType : UserType<Admin>(Dashboard::class, ActiveSearchCriteriaResults::class, ID = -1) {
    override fun reactJSAllowedPages(): Array<String> = arrayOf("/admin")

    override fun reactJSLoginRedirectPage(): String = "/admin/dashboard"

    override fun configureUserTable(userTableBuilder: TableBuilder) {

    }

    override fun confirmationEmailContent(userData: User): EmailContent? = null

    override fun tables(): Array<GenericTableBuilder> = arrayOf()

}