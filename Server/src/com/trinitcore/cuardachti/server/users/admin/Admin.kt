package com.trinitcore.cuardachti.server.users.admin

import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.getUserType
import com.trinitcore.sqlv2.queryUtils.module.Attribute

class Admin(userType: UserType<*>) : User(userType) {
    var username: String by Attribute()
}