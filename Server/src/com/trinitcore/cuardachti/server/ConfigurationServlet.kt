package com.trinitcore.cuardachti.server

import com.trinitcore.asd.Configuration
import com.trinitcore.asd.resourceTools.email.configurations.GoogleMailEmailConfiguration
import com.trinitcore.asd.resourceTools.email.configurations.SMTPEmailConfiguration
import com.trinitcore.asd.resourceTools.fileStorage.FileStorageConfiguration
import com.trinitcore.asd.resourceTools.fileStorage.InternalStorageConfiguration
import com.trinitcore.asd.user.CommonUserType
import com.trinitcore.asd.user.PublicUserType
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.configurations.Column
import com.trinitcore.asd.user.configurations.ColumnType
import com.trinitcore.asd.user.configurations.predefined.PasswordColumn
import com.trinitcore.asd.user.configurations.predefined.UsernameColumn
import com.trinitcore.cuardachti.server.users.PublicType
import com.trinitcore.cuardachti.server.users.admin.AdminType
import com.trinitcore.cuardachti.server.users.adminClient.AdminClientType
import com.trinitcore.cuardachti.server.users.client.ClientType
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.sqlv2.queryUtils.connection.ConnectionManager
import com.trinitcore.sqlv2.queryUtils.connection.PostgresConnectionManager
import javax.servlet.annotation.WebListener
import javax.servlet.http.HttpServlet
import javax.swing.Action

@WebListener
class ConfigurationServlet : HttpServlet() {
    init {
        Configuration.setupConfiguration(C())
    }

    class C : Configuration() {
        override fun projectVersion(): Int = 1

        override fun UTCTimeOffset(): Int = 0

        override fun configureLoginUserTable(userTableBuilder: TableBuilder) {

        }

        override fun databaseConnection(): ConnectionManager = PostgresConnectionManager("localhost","cuardach_ti","postgres","@C[]4m9c17")

        override fun defineActions(): Array<Action> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun defineCommonUserTypes(): Array<CommonUserType> = arrayOf(
                AdminClientType()
        )

        override fun defineEmailConfiguration(): SMTPEmailConfiguration = GoogleMailEmailConfiguration("","","","email_schedule")

        override fun defineFileStorageConfiguration(): FileStorageConfiguration = InternalStorageConfiguration("/files")

        override fun defineLoginColumns(): Array<ColumnType> = arrayOf(
                UsernameColumn(), PasswordColumn()
        )

        override fun definePublicUserType(): PublicUserType = PublicType()

        override fun defineResourcesDirectory(): String = "resources"

        override fun defineUserTypes(): Array<UserType<*>> = arrayOf(
                AdminType(), ClientType()
        )

        override fun projectTitle(): String = "Cuardach Ti"

        override fun resetPasswordLinkSuffix(resetKey: String, userID: Int?): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun websiteLinkPrefix(): String {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }
}