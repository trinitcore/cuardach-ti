package com.trinitcore.cuardachti.server.rest.publicREST

import com.google.gson.JsonObject
import java.util.*
import javax.websocket.OnClose
import javax.websocket.OnMessage
import javax.websocket.OnOpen
import javax.websocket.Session
import javax.websocket.server.ServerEndpoint

@ServerEndpoint("/REST/public/publicSocket")
class PublicSocket {

    companion object {
        val sessions: MutableList<Session> = mutableListOf()

        fun setSearchCriteria(ID: Int, countyJson: JsonObject, townJson: JsonObject, minPrice: Long?, maxPrice: Long?) {
            val message = JsonObject()
            message.addProperty("action","setCriteria")
            message.addProperty("ID",ID)
            message.add("county",countyJson)
            message.add("town",townJson)
            message.addProperty("minPrice",minPrice)
            message.addProperty("maxPrice",maxPrice)

            for (session in sessions) {
                session.basicRemote.sendText(message.toString())
            }
        }

        fun removeSearchCriteria(ID: Int, countyJson: JsonObject, townJson: JsonObject, minPrice: Long?, maxPrice: Long?) {
            val message = JsonObject()
            message.addProperty("action","removeCriteria")
            message.addProperty("ID",ID)
            message.add("county",countyJson)
            message.add("town",townJson)
            message.addProperty("minPrice",minPrice)
            message.addProperty("maxPrice",maxPrice)

            for (session in sessions) {
                session.basicRemote.sendText(message.toString())
            }
        }
    }

    @OnOpen
    fun onOpen(session: Session) {
        sessions.add(session)
    }

    @OnClose
    fun onClose(session: Session) {
        sessions.remove(session)
    }

    @OnMessage
    fun onMessage(json: String) {

    }



}