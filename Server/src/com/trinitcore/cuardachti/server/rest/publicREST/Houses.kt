package com.trinitcore.cuardachti.server.rest.publicREST

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.annotations.WebParameter
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.ErrorStatus
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.asd.servlet.RESTServlet
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.commonUtils.row.Row
import com.trinitcore.sqlv2.commonUtils.row.Rows
import com.trinitcore.sqlv2.queryObjects.SQL
import com.trinitcore.sqlv2.queryObjects.Table
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import java.util.*
import java.util.concurrent.TimeUnit
import javax.servlet.annotation.WebServlet

@WebServlet("/REST/public/houses/*")
class Houses : RESTServlet() {

    lateinit var houses: Table
    lateinit var counties: Table
    lateinit var activesearchcriterias: Table

    // Unique ID | QMap Values
    var valuesToInsert = hashMapOf<String,Array<QMap>>()
    var valuesToUpdate = hashMapOf<String,Array<QMap>>()

    override fun init() {
        super.init()
        Thread {
            flush()
        }.start()
    }

    @Web fun registerTown(townPath: String, townLabel: String, countyLabel: String, source: String) : StatusResult {
        fun inverseSource() = if (source == "myHome") "daft" else "myHome"

        val county = counties.findRow(Where().value("label",countyLabel))

        return if (county != null) {
            val towns = county["towns"] as Rows
            val specificTown = towns.findRowsByColumnValue("label",townLabel).values.firstOrNull() as Row?
            if (specificTown != null) {
                specificTown.update(QMap(source + "TownPath", townPath))
            } else {
                towns.insert(QMap(source + "TownPath",townPath), QMap("label",townLabel))
            }
            OKFormStatus("Registered successfully.")
        } else {
            ErrorStatus("County not found.")
        }
    }

    @Web
    fun getSearchCriterias() : StatusResult {
        return OKStatus("searchCriterias",activesearchcriterias.find())
    }

    @Web fun register(url: String,
                      price: Long,
                      address: String,
                      uniqueIdentifier: String,
                      @WebParameter(optional = true) beds: String?,
                      @WebParameter(optional = true) bathrooms: String?,
                      @WebParameter(optional = true) BER: String?,
                      @WebParameter(optional = true) houseType: String?,
                      @WebParameter(optional = true) dimensions: String?,

                      searchCountyID: Int,
                      searchTownID: Int,
                      @WebParameter(optional = true) searchMinPrice: String?,
                      @WebParameter(optional = true) searchMaxPrice: String?,

                      activeSearchCriteriaID: Int
                      ) : StatusResult {

        val qmaps: Array<QMap> = arrayOf(
                QMap("url", url), QMap("price", price), QMap("address", address), QMap("uniqueIdentifier", uniqueIdentifier),
                QMap("beds", beds), QMap("bathrooms", bathrooms), QMap("BER", BER), QMap("houseType",houseType),

                QMap("searchCountyID",searchCountyID), QMap("searchTownID",searchTownID), QMap("searchMinPrice",searchMinPrice), QMap("searchMaxPrice",searchMaxPrice),

                QMap("entryLastRegisteredTime", System.currentTimeMillis()), QMap("unlisted",false),

                QMap("activeSearchCriteriaID",activeSearchCriteriaID)
        )

        return if (houses.getCount(Where().value("uniqueIdentifier",uniqueIdentifier)) == 0
                && !valuesToInsert.containsKey(uniqueIdentifier)) {
            valuesToInsert[uniqueIdentifier] = qmaps.plus(QMap("initialRegisterTime",System.currentTimeMillis()))
            OKFormStatus("New house registered successfully.")
        } else {
            valuesToUpdate[uniqueIdentifier] = qmaps
            OKFormStatus("House updated successfully.")
        }
    }

    var flushInProgress = false

    private fun flush() {
        flushInProgress = false
        Thread.sleep(TimeUnit.MINUTES.toMillis(1))
        flushInProgress = true
        SQL.session {
            val activeSearchCriteriasData = activesearchcriterias.find()
            // Integrate the newly registered listings cache
            if (valuesToInsert.isNotEmpty())
                houses.multiValueInsert(valuesToInsert.values.toTypedArray())

            // Integrate the updates cache
            for (v in valuesToUpdate) {
                houses.update(Where().value("uniqueIdentifier",v.key), *v.value)
            }

            val specificCriteriaMatch = mutableListOf<Row>()

            // Mark entries at a certain time interval as unlisted
            for (v in houses.find().rowValues()) {
                val specificCriterias = (activeSearchCriteriasData[v["activeSearchCriteriaID"] as Int] as Row)["specificCriterias"] as Rows
                for (specificCriteria in specificCriterias.rowValues()) {
                    val matchingField = v[(specificCriteria["specificField"] as Row)["reflectiveField"]]
                    if (matchingField != null) {
                        specificCriteriaMatch.add(v)
                    }
                }

                if ((v["entryLastRegisteredTime"] as Long) < Date().time - TimeUnit.HOURS.toMillis(24)) {
                    v.update(QMap("unlisted",true))
                }
            }

            dispatchMatchesNotification(specificCriteriaMatch)
        }
        valuesToInsert = hashMapOf()
        valuesToUpdate = hashMapOf()
        flush()
    }

    fun dispatchMatchesNotification(specificCriteriaMatch : List<Row>) {

    }

}