package com.trinitcore.cuardachti.server.rest.admin

import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.sqlv2.queryUtils.module.JSONModule
import org.json.simple.JSONObject

class SpecificField(map: JSONObject) : JSONModule(map) {

    var specificFieldID: Int by Attribute()
    var value: String by Attribute()

}