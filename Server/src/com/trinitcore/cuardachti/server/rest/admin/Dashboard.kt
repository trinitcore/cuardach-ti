package com.trinitcore.cuardachti.server.rest.admin

import kotlin.collections.List

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.annotations.WebParameter
import com.trinitcore.asd.requestTools.Parameters
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.asd.servlet.RESTServlet
import com.trinitcore.cuardachti.server.rest.publicREST.PublicSocket
import com.trinitcore.sqlv2.commonUtils.MultiAssociatingQMap
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.commonUtils.row.Row
import com.trinitcore.sqlv2.commonUtils.row.Rows
import com.trinitcore.sqlv2.queryObjects.Table
import com.trinitcore.sqlv2.queryUtils.module.DataModules
import com.trinitcore.sqlv2.queryUtils.module.RowPool
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import java.util.*
import javax.servlet.annotation.WebServlet
import kotlin.collections.ArrayList
import kotlin.reflect.KClass
import kotlin.reflect.KTypeProjection
import kotlin.reflect.KVariance
import kotlin.reflect.full.*

@WebServlet("/REST/admin/dashboard/*")
class Dashboard : RESTServlet() {

    lateinit var houses: Table
    lateinit var counties: Table
    lateinit var activesearchcriterias: Table
    lateinit var specificfields: Table

    @Web
    fun addSearchCriteria(countyID: Int, townID: Int, @WebParameter(optional = true) minPrice: Number?, @WebParameter(optional = true) maxPrice: Number?) : StatusResult {
        val county = counties.findRowByID(countyID)!!
        val town = (county["towns"] as Rows)[townID] as Row
        val criteria = activesearchcriterias.insert(QMap("countyID",countyID), QMap("townID",townID), QMap("minPrice",minPrice), QMap("maxPrice",maxPrice), QMap("dateTimeLastSet", Date().time))!!

        PublicSocket.setSearchCriteria(criteria.getID(), county.toJsonObject(), town.toJsonObject(), minPrice?.toLong(), maxPrice?.toLong())

        return OKFormStatus("Criteria added.")
    }

    //specificFields: Array<Map<Int,String>>
    //specificFields: Array<JSONObject>
    @Web
    fun setSearchCriteria(ID: Int, countyID: Int, townID: Int, @WebParameter(optional = true) minPrice: Long?, @WebParameter(optional = true) maxPrice: Long?, specificFields: List<SpecificField>) : StatusResult {
        val county = counties.findRowByID(countyID)!!
        val town = (county["towns"] as Rows)[townID] as Row

        PublicSocket.setSearchCriteria(ID, county.toJsonObject(), town.toJsonObject(), minPrice, maxPrice)

        activesearchcriterias.updateByID(ID, QMap("countyID",countyID), QMap("townID",townID), QMap("minPrice",minPrice), QMap("maxPrice",maxPrice), QMap("dateTimeLastSet", Date().time),
                MultiAssociatingQMap("specificCriterias",specificFields).deleteCurrentAssociations())
        return OKFormStatus("Criteria set.")
    }


    @Web
    fun removeSearchCriteria(ID: Int) : StatusResult {
        val criteria = activesearchcriterias.deleteRow(Where().value("ID",ID))!!
        val county = criteria["county"] as Row
        val town = criteria["town"] as Row

        this.houses.delete(Where().value("activeSearchCriteriaID",ID))

        PublicSocket.removeSearchCriteria(ID, county.toJsonObject(), town.toJsonObject(), criteria["minPrice"] as Long?, criteria["maxPrice"] as Long?)

        return OKFormStatus("Criteria terminated successfully.")
    }

    @Web
    fun render() : StatusResult {
        val render = mapOf(
                "searchCriterias" to activesearchcriterias.find().toJSONArray(),
                "counties" to counties.find().toJSONArray(),
                "specificFields" to specificfields.find().toJSONArray()
        )
        return OKStatus(render)
    }

}