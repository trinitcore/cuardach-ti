package com.trinitcore.cuardachti.server.rest.admin

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.asd.servlet.RESTServlet
import com.trinitcore.sqlv2.commonUtils.row.Row
import com.trinitcore.sqlv2.queryObjects.Table
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import javax.servlet.annotation.WebServlet

@WebServlet("/REST/admin/activeSearchCriteriaResults/*")
class ActiveSearchCriteriaResults : RESTServlet() {

    lateinit var houses: Table
    lateinit var specificcriterias: Table

    @Web fun render(ID: Int) : StatusResult {
        val specific = Where()
        specificcriterias.find(Where().value("activeSearchCriteriaID",ID)).rowValues().forEach {
            specific.value((it["specificField"] as Row)["reflectiveField"] as String, it["value"] as String)
        }

        return OKStatus(mapOf(
                "allResults" to houses.find(Where()
                        .value("activeSearchCriteriaID",ID)
                        .value("unlisted",false).join(specific,"AND")
                ).toJSONArray()
        ))
    }

}