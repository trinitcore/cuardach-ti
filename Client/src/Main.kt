import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mashape.unirest.http.Unirest
import com.trinitcore.cuardachti.client.sources.myhome.MyHomeSource
import com.trinitcore.cuardachti.client.sources.tags.County
import com.trinitcore.cuardachti.client.sources.tags.ListingType
import com.trinitcore.cuardachti.client.sources.tags.MaxPrice
import java.util.*
import javax.websocket.*
import com.trinitcore.cuardachti.client.core.Constants
import com.trinitcore.cuardachti.client.sources.daft.DaftSource
import com.trinitcore.cuardachti.client.sources.tags.Town
import org.json.JSONObject
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import java.net.URI
import java.util.concurrent.TimeUnit
import javax.websocket.ContainerProvider
import kotlin.collections.HashMap

@ClientEndpoint
class Main {

    init {
        val container = ContainerProvider.getWebSocketContainer()
        container.connectToServer(this, URI("ws://"+Constants.serverAddress+"/REST/public/publicSocket"))
    }

    lateinit var searchThreads: HashMap<Int,MutableList<Thread>>
    lateinit var myHomeSource: MyHomeSource
    lateinit var daftSource: DaftSource

    private var session: Session? = null

    @OnOpen
    fun onOpen(session: Session) {
        this.session = session

        searchThreads = hashMapOf()
        myHomeSource = MyHomeSource()
        daftSource = DaftSource()

        val response = Unirest.post("http://"+Constants.serverAddress+"/REST/public/houses/getSearchCriterias").asString()
        val jsonResponse = JsonParser().parse(response.body).asJsonObject
        val searchCriterias = jsonResponse["searchCriterias"].asJsonArray

        for (criteria in searchCriterias) {
            setCriteria(criteria.asJsonObject)
        }
    }

    @OnClose
    fun onClose(session: Session) {
        this.session = null
        ceaseAllOperations()
        System.exit(0);
    }

    // fun getThreadAddress(countyLabel: String, townLabel: String, minPrice: Long, maxPrice: Long): String = "$countyLabel|$townLabel|$minPrice|$maxPrice"

    fun ceaseAllOperations() {
        for (searchThread in searchThreads.values) {
            for (thread in searchThread) {
                thread.interrupt()
            }
        }
        searchThreads = hashMapOf()
    }

    @OnMessage
    fun onMessage(json: String) {
        val message = JsonParser().parse(json).asJsonObject
        when (message["action"].asString) {
            "setCriteria" -> {
                setCriteria(message)
            }
            "removeCriteria" -> {
                val ID = message["ID"].asInt
                val county = message["county"] as JsonObject
                val town = message["town"] as JsonObject
                val minPrice = if (!message["minPrice"].isJsonNull) message["minPrice"].asLong else null
                val maxPrice = if (!message["maxPrice"].isJsonNull) message["maxPrice"].asLong else null

                searchThreads[ID]?.let {
                    for (thread in it) {
                        thread.interrupt()
                    }
                }
            }
        }
    }

    fun setCriteria(message: JsonObject) {
        val ID = message["ID"].asInt
        val county = message["county"] as JsonObject
        val town = message["town"] as JsonObject
        val minPrice = if (!message["minPrice"].isJsonNull) message["minPrice"].asLong else null
        val maxPrice = if (!message["maxPrice"].isJsonNull) message["maxPrice"].asLong else null

        searchThreads[ID]?.let {
            for (thread in it) {
                thread.interrupt()
            }
        }

        val myHomeThread = myHomeSource.findHouses(ID,County(county["ID"].asInt, county["myHome"].asString), MaxPrice(maxPrice?.toString() ?: ""), ListingType("residential"), Town(town["ID"].asInt, town["myHomeTownPath"].asString))
        val daftThread = daftSource.findHouses(ID,County(county["ID"].asInt, county["daft"].asString), MaxPrice(maxPrice?.toString() ?: ""), ListingType("residential"), Town(town["ID"].asInt, town["daftTownPath"].asString))
        searchThreads[ID] = mutableListOf(myHomeThread,daftThread)
    }

}

fun registerAllDaftCounties() {
    if (System.getProperty("webdriver.chrome.driver") == null) System.setProperty("webdriver.chrome.driver", "C:\\Users\\Cormac\\Desktop\\chromedriver.exe");
    val ghostDriver = ChromeDriver()

    try {
        ghostDriver.get("http://www.daft.ie/ireland/houses-for-sale/")

        val CSF = ghostDriver.findElementByClassName("search-form")

        val CS = CSF.findElement(By.id("cc_id"))
        val CE =  CSF.findElements(By.tagName("option"))

        fun getElementWithIndex(index: Int): WebElement {
            val countiesSearchForm = ghostDriver.findElementByClassName("search-form")

            val countiesSelector = countiesSearchForm.findElement(By.id("cc_id"))
            val countiesElements =  countiesSelector.findElements(By.tagName("option"))

            return countiesElements[index]
        }

        for ((index,_) in CE.withIndex()) {
            val countiesSearchForm = ghostDriver.findElementByClassName("search-form")
            val countiesSelector = countiesSearchForm.findElement(By.id("cc_id"))
            val searchButton = countiesSearchForm.findElement(By.className("orange-gradient"))
            val countyElement = getElementWithIndex(index)

            val rawCountyLabel = countyElement.text
            val countyLabel = rawCountyLabel.replace("Co. ","")
            if (!countyLabel.contains("--") && !countyLabel.contains("Ireland") && !countyLabel.contains("City")) {
                countiesSelector.click()
                countyElement.click()
                searchButton.click()

                Thread.sleep(3000)

                val townsSearchForm = ghostDriver.findElementByClassName("search-form")

                try {
                    val townsSelector = townsSearchForm.findElement(By.id("a_id"))
                    val townsElements = townsSelector.findElements(By.tagName("option"))

                    townsSelector.click()

                    for (townElement in townsElements) {
                        val townLabel = townElement.text
                        if (!townLabel.contains("-") && townLabel != rawCountyLabel) {
                            println(townLabel)

                            Unirest.post("http://" + Constants.serverAddress + "/REST/public/houses/registerTown")
                                    .field("townPath", townLabel.replace(".", "").replace(" ", "-").toLowerCase())
                                    .field("townLabel", townLabel)
                                    .field("countyLabel", countyLabel)
                                    .field("source", "daft")
                                    .asJson()


                        }
                    }
                    ghostDriver.navigate().back()
                    Thread.sleep(3000)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    } finally {
        ghostDriver.quit()
    }
}

fun registerAllMyHomeCounties() {
    if (System.getProperty("webdriver.chrome.driver") == null) System.setProperty("webdriver.chrome.driver", "C:\\Users\\Cormac\\Desktop\\chromedriver.exe");
    val ghostDriver = ChromeDriver()

    try {
        ghostDriver.get("https://www.myhome.ie/")
        val allButtons = ghostDriver.findElementsByTagName("button")

        var countyButton: WebElement? = null
        var townButton: WebElement? = null

        for (button in allButtons) {
            if (button.getAttribute("ng-click") == "toggleCheckboxes( \$event ); refreshSelectedItems(); refreshButton(); prepareGrouping; prepareIndex();")  {
                val t = button.text
                t
                if (countyButton == null) countyButton = button
                else {
                    townButton = button
                    break
                }
            }
        }
        countyButton!!.click()
        val countiesElements = ghostDriver.findElementByClassName("checkBoxContainer").findElements(By.xpath("//div[@ng-repeat='item in filteredModel | filter:removeGroupEndMarker']"))
        for (countyElement in countiesElements) {
            if (!countyElement.text.contains("--") && (countyElement.text != "")) {
                val countyLabel = countyElement.text.replace(" ","")
                countyElement.click()
                townButton!!.click()

                Thread.sleep(5000)
                println(ghostDriver.findElementsByClassName("checkBoxContainer")[1].getAttribute("innerHTML"))

                val townsElements = ghostDriver.findElementsByClassName("checkBoxContainer")[1].findElements(By.className("my-home-multiSelectItem"))
                for (townElement in townsElements) {
                    if (townElement.text != "") {
                        try {
                            val townLabel = townElement.text.replace("\\d".toRegex(), "").replaceFirst(" ", "")
                            println(townLabel)

                            //townPath: String, townLabel: String, countyLabel: String, source: String

                            Unirest.post("http://" + Constants.serverAddress + "/REST/public/houses/registerTown")
                                    .field("townPath", townLabel.replace(".", "").replace(" ", "-").toLowerCase())
                                    .field("townLabel", townLabel)
                                    .field("countyLabel", countyLabel)
                                    .field("source", "myHome")
                                    .asJson()
                        } catch (e: Exception) {
                            e.printStackTrace()
                            e
                        }
                    }
                }
                countyButton.click()
            }
        }
    } finally {
        ghostDriver.quit()
    }
}

fun main(args: Array<String>) {
    Unirest.setTimeouts(TimeUnit.MINUTES.toMillis(10), TimeUnit.MINUTES.toMillis(10))

    //registerAllDaftCounties()
    Main()
    Scanner(System.`in`).nextLine()
}