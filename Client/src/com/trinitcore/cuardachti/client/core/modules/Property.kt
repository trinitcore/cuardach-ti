package com.trinitcore.cuardachti.client.core.modules

import com.mashape.unirest.http.Unirest
import com.trinitcore.cuardachti.client.core.Constants
import java.util.concurrent.TimeUnit

class Property(val ID: String, val url: String, val websitePrefix: String,
               val price: String, val address: String, val beds: String?, val bathrooms: String?, val BERNumber: String?,
               val houseType: String?, val dimensions: String?,
               val searchCountyID: Int, val searchTownID: Int, val searchMinPrice: String?, val searchMaxPrice: String?,
               val source: String, val activeSearchCriteriaID: Int) {

    fun generateUniqueIdentifier() : String {
        return websitePrefix + ID
    }

    fun mendPrice(price: String) : Long {
        return price.replace("[^\\d.]".toRegex(), "").toLong()
    }

    fun registerData() {
        Unirest.post("http://"+Constants.serverAddress+"/REST/public/houses/register")
                .field("url",url)
                .field("price",mendPrice(price))
                .field("address",address)
                .field("uniqueIdentifier",generateUniqueIdentifier())
                .field("beds",beds)
                .field("bathrooms",bathrooms)
                .field("BER",BERNumber)
                .field("houseType",houseType)
                .field("dimensions",dimensions)

                .field("searchCountyID",searchCountyID)
                .field("searchTownID",searchTownID)
                .field("searchMinPrice",searchMinPrice)
                .field("searchMaxPrice",searchMaxPrice)

                .field("source",source)
                .field("activeSearchCriteriaID",activeSearchCriteriaID)

                .asJson()
    }

}