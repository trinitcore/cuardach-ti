package com.trinitcore.cuardachti.client.core.search

import com.trinitcore.cuardachti.client.core.modules.Property
import com.trinitcore.cuardachti.client.sources.tags.County
import com.trinitcore.cuardachti.client.sources.tags.MaxPrice
import com.trinitcore.cuardachti.client.sources.tags.Town
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities
import kotlin.reflect.KClass

abstract class Source {
    internal abstract val prefixURL: String
    internal abstract val searchURL: String

    abstract fun listingURLs(ghostDriver: ChromeDriver, document: Document) : Array<String>
    abstract fun price(listedPageDocument: Document) : String
    abstract fun address(listedPageDocument: Document) : String
    abstract fun beds(listedPageDocument: Document): String?
    abstract fun bathrooms(listedPageDocument: Document): String?
    abstract fun BERNumber(listedPageDocument: Document, ghostDriver: ChromeDriver): String?
    abstract fun houseType(listedPageDocument: Document): String?
    abstract fun dimensions(listedPageDocument: Document) : String?

    abstract fun ID(listedPageURL: String) : String

    abstract fun awaitPageOfAllListedItems(ghostDriver: ChromeDriver)
    abstract fun awaitListedPage(ghostDriver: ChromeDriver)
    abstract fun goToNextPage(ghostDriver: ChromeDriver, document: Document) : Boolean // Has next page

    inline fun <reified type: SearchTag> findSearchTag(searchTags: Array<out SearchTag>) : type {
        return searchTags.find { it is type}!! as type
    }

    fun findHouses(activeSearchCriteriaID: Int, vararg searchTags: SearchTag) : Thread {
        return Thread {
            if (System.getProperty("webdriver.chrome.driver") == null) System.setProperty("webdriver.chrome.driver", "C:\\Users\\Cormac\\Desktop\\chromedriver.exe");
            val ghostDriver = ChromeDriver()
            fun performOperation() {
                var finalisedURL = "$prefixURL/$searchURL"
                for (tag in searchTags) {
                    tag.initWithClass(this::class)
                    finalisedURL = tag.handleParameterisedValue(finalisedURL)
                }

                val caps = DesiredCapabilities()
                caps.isJavascriptEnabled = true
                caps.setCapability(
                        PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "C:\\Users\\Cormac\\Desktop\\phantomjs.exe"
                )
                caps.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0")

                try {
                    ghostDriver.get(finalisedURL)
                    fun iterate() {
                        var listingURLs: Array<String>? = null
                        var document: Document? = null
                        for (i in 1..10) {
                            try {
                                Thread.sleep(5000)
                                val pageSource = ghostDriver.pageSource
                                document = Jsoup.parse(pageSource)
                                listingURLs = listingURLs(ghostDriver, document)
                                break
                            } catch (e: Exception) {
                                if (i == 10) throw e
                            }
                        }
                        for (listedURL in listingURLs!!) {
                            ghostDriver.get(listedURL)
                            awaitListedPage(ghostDriver)

                            Thread.sleep(500)
                            val listedPageSource = ghostDriver.pageSource
                            val listedDocument = Jsoup.parse(listedPageSource)

                            val property = Property(ID(listedURL), listedURL, prefixURL,
                                    price(listedDocument), address(listedDocument), beds(listedDocument), bathrooms(listedDocument), BERNumber(listedDocument, ghostDriver),
                                    houseType(listedDocument), dimensions(listedDocument),

                                    findSearchTag<County>(searchTags).ID, findSearchTag<Town>(searchTags).ID, null, findSearchTag<MaxPrice>(searchTags).value,
                                    "myHome", activeSearchCriteriaID
                            )
                            property.registerData()
                        }

                        ghostDriver.get(finalisedURL)
                        for (i in 1..10) {
                            try {
                                Thread.sleep(4000)

                                if (goToNextPage(ghostDriver, document!!)) {
                                    finalisedURL = ghostDriver.currentUrl
                                    iterate()
                                } else {
                                    performOperation()
                                }
                                break
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                    iterate()
                } catch (interrupted: InterruptedException) {
                    ghostDriver.quit()
                } finally {
                    Thread {
                        ghostDriver.quit()
                    }.start()
                }
            }
            performOperation()
        }.let { it.start(); it }
    }
 
}