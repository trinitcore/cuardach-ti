package com.trinitcore.cuardachti.client.core.search

import kotlin.reflect.KClass

abstract class SearchTag(val value: String) : Tag() {

    abstract val parameterConditions: Map<KClass<out Source>,String>
    abstract val isURLPatternedParameter: Boolean
    lateinit var parameter: String

    fun initWithClass(kClass: KClass<out Source>) {
        parameter = parameterConditions[kClass]!!
    }

    // Returns a new URL
    open fun handleParameterisedValue(url: String): String {
        return if (!isURLPatternedParameter) {
            url + (if (url.contains("?")) "&" else "?") + parameter + "=" + value
        } else {
            url.replace("^%$parameter%^", value)
        }
    }

}