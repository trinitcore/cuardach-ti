package com.trinitcore.cuardachti.client.sources.daft

import com.trinitcore.cuardachti.client.core.search.Source
import org.jsoup.nodes.Document
import org.openqa.selenium.chrome.ChromeDriver

class DaftSource : Source() {
    override fun awaitPageOfAllListedItems(ghostDriver: ChromeDriver) {

    }

    override fun awaitListedPage(ghostDriver: ChromeDriver) {

    }

    override fun goToNextPage(ghostDriver: ChromeDriver, document: Document) : Boolean {
        if (document.getElementsByClass("next_page").isNotEmpty()) {
            ghostDriver.findElementByClassName("next_page").click()
            return true
        }
        return false
    }

    override fun listingURLs(ghostDriver: ChromeDriver, document: Document): Array<String> {
        return document.getElementsByClass("box").map { this.prefixURL + it.getElementsByTag("a").attr("href") }.toTypedArray()
    }

    override fun price(listedPageDocument: Document): String {
        return listedPageDocument.getElementById("smi-price-string").text()
    }

    override fun address(listedPageDocument: Document): String {
        return listedPageDocument.getElementsByClass("smi-object-header").first().text()
    }

    fun findTrivialElement(listedPageDocument: Document, keyword: String) : String? {
        return listedPageDocument.getElementsByClass("header_text").find { it.text().contains(keyword) }?.text()?.split(" ")?.firstOrNull()
    }

    override fun beds(listedPageDocument: Document): String? {
        return findTrivialElement(listedPageDocument,"bed")
    }

    override fun bathrooms(listedPageDocument: Document): String? {
        return findTrivialElement(listedPageDocument,"baths")
    }

    override fun BERNumber(listedPageDocument: Document, ghostDriver: ChromeDriver): String? {
        return listedPageDocument.getElementsByClass("smi-object-header")[0]?.getElementsByTag("img")?.firstOrNull()?.attr("alt")
    }

    override fun houseType(listedPageDocument: Document): String? {
        return listedPageDocument.getElementsByClass("header_text")[0]?.text()
    }

    override fun dimensions(listedPageDocument: Document): String? {
        return null
    }

    override fun ID(listedPageURL: String): String {
        return listedPageURL.split("/").last()
    }

    override val prefixURL: String
        get() = "https://www.daft.ie"
    override val searchURL: String
        get() = "^%county%^/houses-for-sale/^%town%^/"

}