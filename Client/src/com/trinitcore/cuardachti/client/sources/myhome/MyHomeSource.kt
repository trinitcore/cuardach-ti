package com.trinitcore.cuardachti.client.sources.myhome

import com.trinitcore.cuardachti.client.core.search.Source
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.openqa.selenium.chrome.ChromeDriver

class MyHomeSource : Source() {
    override fun awaitPageOfAllListedItems(ghostDriver: ChromeDriver) {
        /*
        while (true) {
            try {
                ghostDriver.findElementByClassName("loader ng-hide")
            } catch (e: Exception) {
                break
            }
        }
        */
        Thread.sleep(1500)
    }

    override fun awaitListedPage(ghostDriver: ChromeDriver) {
        while (true) {
            try {
                ghostDriver.findElementByClassName("text")
                break
            } catch (e: Exception) {

            }
        }
    }

    override fun goToNextPage(ghostDriver: ChromeDriver, document: Document) : Boolean {
        if (document.getElementById("next") != null && document.getElementById("next").attr("disabled") != "disabled") {
            ghostDriver.findElementById("next").click()
            awaitPageOfAllListedItems(ghostDriver)
            return true
        }
        return false
    }

    override fun listingURLs(ghostDriver: ChromeDriver, document: Document): Array<String> {
        val a= document.getElementsByClass("mhPropertyList")[0].getElementsByAttributeValue("ng-repeat","property in vm.response.SearchResults track by property.id | limitTo : 20")
        return a.map { it.getElementsByTag("a")[0].attr("href") }.toTypedArray()
    }

    override fun price(listedPageDocument: Document): String {
        return listedPageDocument.getElementsByAttributeValue("ng-if","!vm.propertyDetails.NewHomePriceString")[0].text()
    }

    override fun address(listedPageDocument: Document): String {
        return listedPageDocument.getElementsByClass("address")[0].text()
    }

    override fun beds(listedPageDocument: Document): String? {
        val a = listedPageDocument.getElementsByAttributeValue("ng-if", "vm.propertyDetails.Property.BedsString && !vm.propertyDetails.Property.NewHomeBedString")
        return if (!a.isEmpty()) a[0].text() else null
    }

    override fun bathrooms(listedPageDocument: Document): String? {
        val a = listedPageDocument.getElementsByAttributeValue("ng-if","vm.propertyDetails.Property.Bathrooms")
        return if (!a.isEmpty()) a[0].text() else null
    }

    override fun BERNumber(d: Document, ghostDriver: ChromeDriver): String? {
        val listedPageDocument = if (d.select("button.toggleTruncateText").isNotEmpty()) {
            ghostDriver.findElementByClassName("toggleTruncateText").click()
            Thread.sleep(100)
            Jsoup.parse(ghostDriver.pageSource)
        } else d

        var addNextElement = false
        val text = mutableListOf<String>()
        val html = listedPageDocument.getElementsByClass("description")[0].html()

        val split = html.split("\n")
        split.forEach {
            if (addNextElement) {
                if (it.contains("h2")) addNextElement = false
                else {
                    text.add(it.replace("<[^>]*>".toRegex(),""))
                }
            } else if (it.contains("h2") && it.contains("BER")) {
                addNextElement = true
            }
        }
        return if (text.isEmpty()) null else text.joinToString(" ")
    }

    override fun houseType(listedPageDocument: Document): String? {
        val a = listedPageDocument.getElementsByAttributeValue("ng-if","vm.propertyDetails.Property.PropertyType")
        return if (!a.isEmpty()) a[0].text() else null
    }

    override fun dimensions(listedPageDocument: Document): String? {
        val a = listedPageDocument.getElementsByAttributeValue("ng-if","vm.propertyDetails.Property.SizeStringMeters && !vm.propertyDetails.Property.NewHomeSizeString")
        return if (!a.isEmpty()) a[0].text() else null
    }

    override fun ID(listedPageURL: String): String {
        return listedPageURL.split("/").last()
    }

    override val prefixURL: String
        get() = "https://www.myhome.ie"
    override val searchURL: String
        get() = "^%listingType%^/^%county%^/property-for-sale-in-^%town%^"


}