package com.trinitcore.cuardachti.client.sources.tags

import com.trinitcore.cuardachti.client.core.search.SearchTag
import com.trinitcore.cuardachti.client.core.search.Source
import com.trinitcore.cuardachti.client.sources.daft.DaftSource
import com.trinitcore.cuardachti.client.sources.myhome.MyHomeSource
import kotlin.reflect.KClass

class ListingType(value: String) : SearchTag(value) {
    override val isURLPatternedParameter: Boolean
        get() = true
    override val parameterConditions: Map<KClass<out Source>, String>
        get() = mapOf(
                MyHomeSource::class to "listingType",
                DaftSource::class to "listingType"
        )
}