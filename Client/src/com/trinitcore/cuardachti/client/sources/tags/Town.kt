package com.trinitcore.cuardachti.client.sources.tags

import com.trinitcore.cuardachti.client.core.search.SearchTag
import com.trinitcore.cuardachti.client.core.search.Source
import com.trinitcore.cuardachti.client.sources.daft.DaftSource
import com.trinitcore.cuardachti.client.sources.myhome.MyHomeSource
import kotlin.reflect.KClass

class Town(val ID: Int, value: String) : SearchTag(value) {
    override val parameterConditions: Map<KClass<out Source>, String>
        get() = mapOf(
                MyHomeSource::class to "town",
                DaftSource::class to "town"
        )
    override val isURLPatternedParameter: Boolean
        get() = true
}